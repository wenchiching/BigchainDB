# Separate all crypto code so that we can easily test several implementations

import sha3
from cryptoconditions import crypto
from bigchaindb import keyUtils
import base58


def hash_data(data):
    """Hash the provided data using SHA3-256"""
    return sha3.sha3_256(data.encode()).hexdigest()


def generate_key_pair():
    public_key, private_key = keyUtils.gen_ecdsa_pair()
    public_key = base58.b58encode(public_key)
    private_key = base58.b58encode(private_key)
    return public_key, private_key

SigningKey = crypto.Ed25519SigningKey
VerifyingKey = crypto.Ed25519VerifyingKey
